# Toggl Burnup

- [Toggl Burnup](#toggl-burnup)
- [Introducción](#introducción)
- [Prerequisitos](#prerequisitos)
- [Uso](#uso)
- [Opciones](#opciones)
- [Problemas enfrentados](#problemas-enfrentados)
- [Conclusiones](#conclusiones)



# Introducción

Este programa tiene como finalidad probar por una parte Typescript y por otra Deno, además de proporcionar una herramienta que vaya a usar en mi día a día. Además como comparación este programa tiene versión [NodeJS](https://gitlab.com/Nisgrak/toggl-burnup-nodejs) y [Deno](https://gitlab.com/Nisgrak/toggl-burnup-deno).

# Prerequisitos
1. Instalar las dependencias con:
```sh
npm install
# or
yarn install
```
3. Rellenar la variable `TOGGL_TOKEN` del archivo `.env.example` con el [api token de toggl](https://toggl.com/app/profile) y renombrar a `.env`

# Uso
Para ejecutar el programa se realizará con el comando:
```sh
npm run start [-- <args>] # The -- is mandatory with npm to add params
# or
yarn start <args>
```

El programa irá copiando al portapapeles cada columna necesaria del burnup por días, dando enter entre cada paso.

# Opciones

| Flag        | Descripción                            | Formato | Valor por defecto |
| ----------- | -------------------------------------- | ------- | ----------------- |
| help        | Muestra la ayuda                       | -       | -                 |
| --start, -s | La fecha desde donde empezará a buscar | DD[/MM] | Hoy               |
| --end, -e   | La fecha donde acabará a buscar        | DD[/MM] | Hoy               |

# Problemas enfrentados

La idea original del proyecto ha ido pasando por diversas fases, a destacar:
1. Lanzarme a usar Deno y afrontar muchos paquetes desactualizados, desconomiento de TS, errores varios por ser inestable Deno...
2. Hacerlo en NodeJS y en JS plano, además de desechar la idea principal de modificar el archivo xlsm ya que todas las librerías no guardan los gráficos existentes en el archivo.
3. Una vez finalizado, integrarlo con TS aplicado a NodeJS, siendo mucho más sencillo esta vez ya que estaba más madura la idea.
4. Aún habiendo descartado todo uso de Deno decido probar una ultima vez, consiguiendo ahora integrarlo de forma satisfactoria.

# Conclusiones
Al ser la primera vez que uso Typescript he visto que es un paso más de complejidad aplicado a Javascript, si bien es muy cómodo de usar en NodeJs, ya que la gran mayoría de librerías tienen compatibilidad y tienes un entorno que te avisa de malas prácticas o posibles errores derivados.

Por su parte creo que Deno le falta mucho, y cuando digo mucho es mucho, recorrido. Ya no solo porque hay pocas librerías, y las que hay quizá no están actualizadas o tienen errores, si no porque faltan muchas cosas cruciales como pegar al portapapeles de forma nativa(la librería que uso ejecuta un comando en consola, y no funciona en linux...)

Pero debo decir que es muy sencillo de empezar a trabajar con él, ya que para NodeJS hay que montar mucho "boilerplate" y con diferentes paquetes, aquí todo se hace automático con un click y nativo, no hay color en usar TS.
