export default {
    TogglApi: process.env.TOGGL_API ?? "",
    TogglToken: process.env.TOGGL_TOKEN ?? "",
    TogglPassword: process.env.TOGGL_PASSWORD ?? "",
    PointToMinutes: parseInt(process.env.POINT_TO_MINUTES ?? "", 10)
}