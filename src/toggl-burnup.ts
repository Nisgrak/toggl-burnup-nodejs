import dotenv from "dotenv";
dotenv.config();
import chalk from "chalk";
import { program } from "commander";
import dayjs from 'dayjs';
import 'dayjs/locale/es';
dayjs.locale("es");
import standardDay from "./utils/standardDay.js"
import groupBy from "./utils/groupBy.js";
import pjson from 'pjson';


async function init() {

	const TogglRecord = require("./models/TogglRecord.js");

	program
		.version(pjson.version)
		.option('-s, --start DD[/MM]', 'start date. Default to today')
		.option('-e, --end DD[/MM]', 'end date. Default to today')
		.addHelpCommand('help', 'show assistance');

	program.parse(process.argv);

	program.start = standardDay(program.start, 0);
	program.end = standardDay(program.end, 23);

	console.log(chalk.bold("Fecha de inicio:"), chalk.yellow(program.start.format('DD/MM/YYYY HH:mm')));
	console.log(chalk.bold("Fecha de fin:"), chalk.yellow(program.end.format('DD/MM/YYYY HH:mm')));

	const recordList = await TogglRecord.getList(program.start.format(), program.end.format());

	const listOrderedByDay = groupBy(recordList, "FormatDate");

	const listOrderedByDayKeys = Object.keys(listOrderedByDay);
	for (const day of Object.keys(listOrderedByDay)) {
		const recordsDay = listOrderedByDay[day];
		const hasNextDay = listOrderedByDayKeys[listOrderedByDayKeys.length - 1] !== day;
		console.log("")

		await TogglRecord.generateDay(recordsDay, hasNextDay);

		if (hasNextDay) {
			console.log("--------------")
		}
	}


}

init();