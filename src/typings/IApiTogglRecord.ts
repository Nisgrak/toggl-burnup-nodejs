export default interface ApiTogglRecord {
    id: number,
    guid: string,
    wid: number,
    pid: number,
    billable: boolean,
    start: string,
    stop: string,
    duration: number,
    description?: string,
    tags?: string[],
    duronly: boolean,
    at: string,
    uid: number
}