export default <T>(list: T[], functionName: string): Record<string, T[]> => {
	return list.reduce((acc, value: any) => {
		acc[value[functionName]] = acc[value[functionName]] || [];
		acc[value[functionName]].push(value);
		return acc;
	}, Object.assign({}));
}