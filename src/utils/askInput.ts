import readline from 'readline';

export default (question: string) => {
	const rl = readline.createInterface({
		input: process.stdin,
		output: process.stdout,
	});

	return new Promise(resolve => rl.question(question, (ans: string) => {
		rl.close();
		resolve(ans);
	}))
}