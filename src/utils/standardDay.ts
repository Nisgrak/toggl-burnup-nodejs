import dayjs from "dayjs";

export default (dateString: string, hour: number) => {

	let dateObject = dayjs();

	if (dateString) {
		const [day, month] = dateString.split("/");
		if (month) {
			dateObject = dateObject.month(parseInt(month, 10) - 1);
		}

		dateObject = dateObject.date(parseInt(day, 10));
	}

	dateObject = dateObject.hour(hour);
	dateObject = dateObject.minute(0);

	return dateObject;
}