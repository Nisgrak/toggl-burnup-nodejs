import axios from "axios";
import chalk from "chalk";
import clipboardy from "clipboardy";
import askInput from "../utils/askInput.js";
import groupBy from "../utils/groupBy.js";
import dayjs from 'dayjs'
import ApiTogglRecord from "../typings/IApiTogglRecord.js";
import config from "../env.config";

class TogglRecord {
    #duration: number;
    #name: string;
    #ot: string;
    #date: dayjs.Dayjs;

    constructor(record: ApiTogglRecord) {
        this.#duration = Math.round(record.duration / (config.PointToMinutes * 60));
        this.#name = record.description || "";
        this.#date = dayjs(record.start);

        const otTag = record.tags?.find(tag => /^OT-\d+ \//.test(tag));

        if (otTag) {
            this.#ot = otTag;
        } else {
            this.#ot = "Comments";
        }
    }

    get Duration() {
        return this.#duration;
    }

    get Name() {
        return this.#name;
    }

    get Ot() {
        return this.#ot;
    }

    get OtNum() {
        return this.#ot.split("/")[0];
    }

    get Date() {
        return this.#date;
    }

    get FormatDate() {
        return this.#date.format('DD/MM/YYYY');
    }

    get FormatName() {
        return `${this.OtNum}/ ${this.Name}`
    }

    static async getList(startDate: string, endDate: string) {
        const result = await axios.get(config.TogglApi + "time_entries", {
            params: {
                start_date: startDate,
                end_date: endDate
            },
            auth: {
                username: config.TogglToken,
                password: config.TogglPassword
            },
        })

        const togglRecords = result.data;

        return togglRecords.map((record: ApiTogglRecord) => new TogglRecord(record));
    }

    static async generateDay(recordList: TogglRecord[], waitToCopyComments: boolean) {
        const day = recordList[0].FormatDate;
        let dayPoints = 0;
        let otTasks = "";
        let comments = "";

        const recordsByName = groupBy(recordList, "FormatName");

        for (const tag of Object.keys(recordsByName)) {
            const recordsByTag = recordsByName[tag];
            const duration = recordsByTag.reduce((acc, value) => acc + value.Duration, 0);

            if (tag.startsWith("Comments/")) {
                if (comments !== "") {
                    comments += "\n";
                }

                comments += `${recordsByTag[0].Name} (${duration})`
            } else {
                if (otTasks !== "") {
                    otTasks += "\n";
                }

                otTasks += `${recordsByTag[0].FormatName} (${duration})`

            }

            dayPoints += duration;
        }

        await TogglRecord.showDay(day, dayPoints, otTasks, comments, waitToCopyComments);
    }

    static async showDay(day: string, dayPoints: number, otTasks: string, comments: string, waitToCopyComments: boolean) {
        console.log(chalk.yellow("Día " + day))

        await clipboardy.write(dayPoints.toString());
        console.log("%s %s", chalk.green("Se han copiado tus puntos del día:"), chalk.magenta(dayPoints));
        await askInput(chalk.blue("Presiona cualquier enter para continuar.\n"));

        if (otTasks !== "") {
            await clipboardy.write("\"" + otTasks + "\"");
            console.log("%s\n%s\n", chalk.green("Se han copiado tus tareas del día: "), chalk.magenta(otTasks));
            await askInput(chalk.blue("Presiona cualquier enter para continuar.\n"));

        } else {
            console.log(chalk.red("No tienes ninguna tareas para el día.\n"))
        }

        if (comments !== "") {
            await clipboardy.write("\"" + comments + "\"");
            console.log("%s\n%s", chalk.green("Se han copiado tus comentarios del día:"), chalk.magenta(comments));

            if (waitToCopyComments) {
                await askInput(chalk.blue("Presiona cualquier enter para continuar.\n"));

            }
        } else {
            console.log("%s\n", chalk.red("No tienes ningún comentario para el día."))
        }
    }
}

module.exports = TogglRecord;