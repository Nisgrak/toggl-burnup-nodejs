"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const chalk_1 = __importDefault(require("chalk"));
const commander_1 = require("commander");
const dayjs_1 = __importDefault(require("dayjs"));
require("dayjs/locale/es");
dayjs_1.default.locale("es");
const standardDay_js_1 = __importDefault(require("./utils/standardDay.js"));
const groupBy_js_1 = __importDefault(require("./utils/groupBy.js"));
const pjson_1 = __importDefault(require("pjson"));
function init() {
    return __awaiter(this, void 0, void 0, function* () {
        const TogglRecord = require("./models/TogglRecord.js");
        commander_1.program
            .version(pjson_1.default.version)
            .option('-s, --start DD[/MM]', 'start date. Default to today')
            .option('-e, --end DD[/MM]', 'end date. Default to today')
            .addHelpCommand('help', 'show assistance');
        commander_1.program.parse(process.argv);
        commander_1.program.start = standardDay_js_1.default(commander_1.program.start, 0);
        commander_1.program.end = standardDay_js_1.default(commander_1.program.end, 23);
        console.log(chalk_1.default.bold("Fecha de inicio:"), chalk_1.default.yellow(commander_1.program.start.format('DD/MM/YYYY HH:mm')));
        console.log(chalk_1.default.bold("Fecha de fin:"), chalk_1.default.yellow(commander_1.program.end.format('DD/MM/YYYY HH:mm')));
        const recordList = yield TogglRecord.getList(commander_1.program.start.format(), commander_1.program.end.format());
        const listOrderedByDay = groupBy_js_1.default(recordList, "FormatDate");
        const listOrderedByDayKeys = Object.keys(listOrderedByDay);
        for (const day of Object.keys(listOrderedByDay)) {
            const recordsDay = listOrderedByDay[day];
            const hasNextDay = listOrderedByDayKeys[listOrderedByDayKeys.length - 1] !== day;
            console.log("");
            yield TogglRecord.generateDay(recordsDay, hasNextDay);
            if (hasNextDay) {
                console.log("--------------");
            }
        }
    });
}
init();
