"use strict";
var _a, _b, _c, _d;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    TogglApi: (_a = process.env.TOGGL_API) !== null && _a !== void 0 ? _a : "",
    TogglToken: (_b = process.env.TOGGL_TOKEN) !== null && _b !== void 0 ? _b : "",
    TogglPassword: (_c = process.env.TOGGL_PASSWORD) !== null && _c !== void 0 ? _c : "",
    PointToMinutes: parseInt((_d = process.env.POINT_TO_MINUTES) !== null && _d !== void 0 ? _d : "", 10)
};
