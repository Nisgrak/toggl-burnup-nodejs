"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _duration, _name, _ot, _date;
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const chalk_1 = __importDefault(require("chalk"));
const clipboardy_1 = __importDefault(require("clipboardy"));
const askInput_js_1 = __importDefault(require("../utils/askInput.js"));
const groupBy_js_1 = __importDefault(require("../utils/groupBy.js"));
const dayjs_1 = __importDefault(require("dayjs"));
const env_config_1 = __importDefault(require("../env.config"));
class TogglRecord {
    constructor(record) {
        var _a;
        _duration.set(this, void 0);
        _name.set(this, void 0);
        _ot.set(this, void 0);
        _date.set(this, void 0);
        __classPrivateFieldSet(this, _duration, Math.round(record.duration / (env_config_1.default.PointToMinutes * 60)));
        __classPrivateFieldSet(this, _name, record.description || "");
        __classPrivateFieldSet(this, _date, dayjs_1.default(record.start));
        const otTag = (_a = record.tags) === null || _a === void 0 ? void 0 : _a.find(tag => /^OT-\d+ \//.test(tag));
        if (otTag) {
            __classPrivateFieldSet(this, _ot, otTag);
        }
        else {
            __classPrivateFieldSet(this, _ot, "Comments");
        }
    }
    get Duration() {
        return __classPrivateFieldGet(this, _duration);
    }
    get Name() {
        return __classPrivateFieldGet(this, _name);
    }
    get Ot() {
        return __classPrivateFieldGet(this, _ot);
    }
    get OtNum() {
        return __classPrivateFieldGet(this, _ot).split("/")[0];
    }
    get Date() {
        return __classPrivateFieldGet(this, _date);
    }
    get FormatDate() {
        return __classPrivateFieldGet(this, _date).format('DD/MM/YYYY');
    }
    get FormatName() {
        return `${this.OtNum}/ ${this.Name}`;
    }
    static getList(startDate, endDate) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield axios_1.default.get(env_config_1.default.TogglApi + "time_entries", {
                params: {
                    start_date: startDate,
                    end_date: endDate
                },
                auth: {
                    username: env_config_1.default.TogglToken,
                    password: env_config_1.default.TogglPassword
                },
            });
            const togglRecords = result.data;
            return togglRecords.map((record) => new TogglRecord(record));
        });
    }
    static generateDay(recordList, waitToCopyComments) {
        return __awaiter(this, void 0, void 0, function* () {
            const day = recordList[0].FormatDate;
            let dayPoints = 0;
            let otTasks = "";
            let comments = "";
            const recordsByName = groupBy_js_1.default(recordList, "FormatName");
            for (const tag of Object.keys(recordsByName)) {
                const recordsByTag = recordsByName[tag];
                const duration = recordsByTag.reduce((acc, value) => acc + value.Duration, 0);
                if (tag.startsWith("Comments/")) {
                    if (comments !== "") {
                        comments += "\n";
                    }
                    comments += `${recordsByTag[0].Name} (${duration})`;
                }
                else {
                    if (otTasks !== "") {
                        otTasks += "\n";
                    }
                    otTasks += `${recordsByTag[0].FormatName} (${duration})`;
                }
                dayPoints += duration;
            }
            yield TogglRecord.showDay(day, dayPoints, otTasks, comments, waitToCopyComments);
        });
    }
    static showDay(day, dayPoints, otTasks, comments, waitToCopyComments) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(chalk_1.default.yellow("Día " + day));
            yield clipboardy_1.default.write(dayPoints.toString());
            console.log("%s %s", chalk_1.default.green("Se han copiado tus puntos del día:"), chalk_1.default.magenta(dayPoints));
            yield askInput_js_1.default(chalk_1.default.blue("Presiona cualquier enter para continuar.\n"));
            if (otTasks !== "") {
                yield clipboardy_1.default.write("\"" + otTasks + "\"");
                console.log("%s\n%s\n", chalk_1.default.green("Se han copiado tus tareas del día: "), chalk_1.default.magenta(otTasks));
                yield askInput_js_1.default(chalk_1.default.blue("Presiona cualquier enter para continuar.\n"));
            }
            else {
                console.log(chalk_1.default.red("No tienes ninguna tareas para el día.\n"));
            }
            if (comments !== "") {
                yield clipboardy_1.default.write("\"" + comments + "\"");
                console.log("%s\n%s", chalk_1.default.green("Se han copiado tus comentarios del día:"), chalk_1.default.magenta(comments));
                if (waitToCopyComments) {
                    yield askInput_js_1.default(chalk_1.default.blue("Presiona cualquier enter para continuar.\n"));
                }
            }
            else {
                console.log("%s\n", chalk_1.default.red("No tienes ningún comentario para el día."));
            }
        });
    }
}
_duration = new WeakMap(), _name = new WeakMap(), _ot = new WeakMap(), _date = new WeakMap();
module.exports = TogglRecord;
