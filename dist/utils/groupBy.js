"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (list, functionName) => {
    return list.reduce((acc, value) => {
        acc[value[functionName]] = acc[value[functionName]] || [];
        acc[value[functionName]].push(value);
        return acc;
    }, Object.assign({}));
};
