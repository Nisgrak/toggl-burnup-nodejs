"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dayjs_1 = __importDefault(require("dayjs"));
exports.default = (dateString, hour) => {
    let dateObject = dayjs_1.default();
    if (dateString) {
        const [day, month] = dateString.split("/");
        if (month) {
            dateObject = dateObject.month(parseInt(month, 10) - 1);
        }
        dateObject = dateObject.date(parseInt(day, 10));
    }
    dateObject = dateObject.hour(hour);
    dateObject = dateObject.minute(0);
    return dateObject;
};
